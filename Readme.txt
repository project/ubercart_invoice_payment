README.txt
==========

This module enables the owner of a site to give an option to the user to make the purchase first and make the payment for the purchase later.
Till now, Ubercart only enables the user to make the payment first for a product in drupal 7.
There is no way to create an Invoice for a product and track it till its payment is done by a user.

This module is an sub-module of Ubercart.

A module that help a user to pay for a product after delivery.
This module is very helpful for site owner to send invoice to the user/client
and allow him/her to pay after getting the product.

Lets taking an scenerio to understand the benefits of this module:

"If an admin/owner of an E-commerce site wants to create a order for him/her
friends,relatives etc.
Then he/she go to store of the site and create an order for this. ('This is the great functionality provide by ubercart')
But there is no way in the drupal 7 to create a payment for this order after creating it.
If an admin/owner of an site wants that to create an order for a user,
and after delivery of product, that user 
"

