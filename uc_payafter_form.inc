<?php

/**
 * @file
 * Invoice user menu items.
 * Explanation of all functioalities of all user menu items.
 */

/**
 * List of Invoices of particular Invoice.
 * Show the 'invoice/list' screen
 */

function uc_payafter_user_invoice_list_view() {
    global $base_url;
    global $user;
    $result = array();
    $where_clause = " where user_id=" . $user->uid;
    $sql = db_query("SELECT * FROM {uc_payafter}" . $where_clause . " GROUP BY invoice_id");
    $result['products'] = array(
        '#prefix' => '<div id="custom-payafter-user-view-product">',
        '#suffix' => '</div>',
        '#tree'   => TRUE,
    );
    $result['products'] += tapir_get_table('uc_payafter_view_table');
    $i = 0;
    while ($sql_result = $sql->fetchAssoc()) {
    // Get the Total Price of Invoice
    $sql1 = db_query('SELECT SUM(total_price) FROM {uc_payafter} WHERE invoice_id=' . $sql_result['invoice_id'])->fetchAssoc();
    $uid = $sql_result['user_id'];
    $user_info = user_load($uid);
    if (($sql_result['c_status'] == 0)) {
        $status = 'Pending';
    }
    elseif ($sql_result['c_status'] == 1) {
        $status = 'Complete';
    }
    $result['products'][$i]['actions'] = array(
        '#type' => 'markup',
        '#markup' => '<a title="View Invoice" href="' . $base_url . '/invoice/' . $sql_result['invoice_id'] . '/view"><img alt="View Invoice" src="' . $base_url . '/sites/all/modules/uc_payafter/images/order_view.png" typeof="foaf:Image"></a>',
    );
    $result['products'][$i]['invoice_id'] = array(
        '#type' => 'markup',
        '#markup' => '<a title="View Invoice" href="' . $base_url . '/invoice/' . $sql_result['invoice_id'] . '/view">' . $sql_result['invoice_id'] . '</a>',
    );
    $result['products'][$i]['customer'] = array(
        '#type' => 'markup',
        '#markup' => '<a title="View User Profile" href="' . $base_url . '/user/' . $sql_result['user_id'] . '">' . $user_info->name . '</a>',
    );
    $result['products'][$i]['total_price'] = array(
        '#type' => 'markup',
        '#markup' => $sql1['sum(total_price)'],
    );
    $result['products'][$i]['date'] = array(
        '#type' => 'markup',
        '#markup' => date('d/m/Y', $sql_result['created']),
    );
    $result['products'][$i]['status'] = array(
        '#type' => 'markup',
        '#markup' => $status,
    );
    $++i;
    }
    return $result;    
}
 /**
 * Show Particular invoice belonged to Current User.
 * Show the 'invoice/list' screen
 */
function uc_payafter_user_invoice_view($invoice) {
    global $base_url;
    $query = db_select('uc_payafter', 'u')
        ->fields('u')
        ->condition('u.invoice_id', $invoice, '=')
        ->execute()
        ->fetchAll();
    if (count($query)>0) {
        $query1 = db_select('uc_payafter', 'u')
            ->fields('u')
            ->condition('u.status', '1', '=')
            ->condition('u.invoice_id', $invoice, '=')
            ->execute()
            ->fetchAll();
        $result['products'] = array(
            '#prefix' => '<div id="custom-payafter-view-invoice-product">',
            '#suffix' => '</div>',
            '#tree'   => TRUE,
        );
        $result['products'] += tapir_get_table('uc_payafter_particular_view_table');
        $total_invoice = 0;
        $value1 = array();
        foreach ($query1 as $key => $value) {
            $value1 = $value;
            $total_invoice += $value->total_price;
            $node_info = node_load($value->product_id);
            $result['products'][$key]['qty'] = array(
                '#type' => 'markup',
                '#markup' => $value->quantity,
            );
            $result['products'][$key]['product'] = array(
                '#type' => 'markup',
                '#markup' => '<a href = "' . $base_url . '/node/' . $node_info->nid . '">' . $node_info->title . '</a>',
            );
            $result['products'][$key]['sku'] = array(
                '#type' => 'markup',
                '#markup' => $node_info->model,
            );
            $result['products'][$key]['price'] = array(
                '#type' => 'markup',
                '#markup' => $value->price_per_product,
            );
            $result['products'][$key]['total'] = array(
                '#type' => 'markup',
                '#markup' => $value->total_price,
            );
        }
        $result['total'] = array(
            '#prefix' => '<div id="custom-payafter-view-invoice-total">',
            '#suffix' => '</div>',
            '#tree'   => TRUE,
        );
        $result['total'] += array(
            '#type' => 'fieldset',
        );
        $result['total']['invoice'] = array(
            '#type' => 'markup',
            '#markup' => '<b>Subtotal</b>: ' . $total_invoice . '<br/>',
        );
        $query1 = db_select('uc_payafter_line_item', 'u')
        ->fields('u')
        ->condition('u.invoice_id', $invoice, '=')
        ->execute()
        ->fetchAll();
      if (count($query1)>0) {
         foreach ($query1 as $key => $value) {
            $total_invoice += $value->amount;
            $result['total']['line-item' . $key] = array(
               '#type' => 'markup',
               '#markup' => '<b>' . $value->title . '</b> : ' . $value->amount . '<br/>',
            );
         }
      }
      $result['total']['invoice-total'] = array(
         '#type' => 'markup',
         '#markup' => '<b>Total</b> : ' . $total_invoice,
      );
    }
    else {
        drupal_set_message(t('There is no such Invoice exist.'));
        drupal_goto($base_url);
    }
    $final_result = $result;
    if ($value1->c_status == 0) {
        $form = drupal_get_form('uc_payafter_user_invoice_view_form', $invoice);
        $final_result = array_merge($result, $form);
    }
    else {
      drupal_set_message(t('This Invoice is Completed'));
    }
    return $final_result;
}
 /**
 * Create a form for add the invoice item into the cart and redirect to the check out page.
 * 
 * @see uc_payafter_user_invoice_view_form_submit()
 * @ingroup forms
 */
function uc_payafter_user_invoice_view_form($from, &$form_state, $invoice) {
    $form = array();
    $form['invoice_id'] = array(
        '#type' => 'hidden',
        '#value' => $invoice,
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Proceed to Payment',
    );
    return $form;
}

 /**
 * Form Submission handler for uc_payafter_user_invoice_view_form.
 * 
 * @, see uc_payafter_user_invoice_view_form_submit()
 * @ingroup forms
 */
function uc_payafter_user_invoice_view_form_submit($form, &$form_state) {
    $invoice = $form_state['values']['invoice_id'];
    $query1 = db_select('uc_payafter', 'u')
        ->fields('u')
        ->condition('u.status', '1', '=')
        ->condition('u.invoice_id', $invoice, '=')
        ->execute()
        ->fetchAll();
    foreach ($query1 as $key => $value) {
        uc_cart_add_item($value->product_id, $qty = $value->quantity);
    }
    $_SESSION['invoice'] = $invoice;
    $_SESSION['line_item'] = 0;
    drupal_goto('cart/checkout');
}